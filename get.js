const { OK, NOT_FOUND } = require('http-status-codes')
const AWS = require('aws-sdk')

const dynamoDb = new AWS.DynamoDB.DocumentClient()

const run = async event => {
  const record = {
    TableName: 'todos',
    Key: {
      id: event.pathParameters.id
    }
  }

  const result = await dynamoDb.get(record).promise()
  if (result.Item) {
    return {
      statusCode: OK,
      body: JSON.stringify(result.Item)
    }
  } else {
    return {
      statusCode: NOT_FOUND,
      body: JSON.stringify({ message: 'Couldn\'t find the todo item.' })
    }
  }
}

module.exports.run = run
