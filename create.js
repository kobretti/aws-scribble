const generateId = require('uuid/v4')
const { OK }= require('http-status-codes')
const AWS = require('aws-sdk')

const documentClient = new AWS.DynamoDB.DocumentClient()

module.exports.run = async event => {
  const { text } = JSON.parse(event.body)
  const record = {
    TableName: 'todos',
    Item: {
      id: generateId(),
      text,
      checked: false
    }
  }

  await documentClient.put(record).promise()

  return {
    statusCode: OK,
    body: JSON.stringify(record.Item)
  }
}
